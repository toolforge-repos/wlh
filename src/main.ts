import { createApp, ref } from 'vue';

import 'normalize.css/normalize.css';
import '@wikimedia/codex/dist/codex.style.css';
import './assets/style.less';

import App from './App.vue';

import { router } from './router';
import { Namespace } from './types';
import { NamespacesKey } from './keys';
import { fetchEnWikiNamespaces } from './utils';

// Set up some data before the app mounts so it can be provided globally --------------------------
const namespaces = ref<Namespace[]>( [] );
fetchEnWikiNamespaces().then( ( data ) => {
	namespaces.value = data;
} );

// Create and mount the Vue app -------------------------------------------------------------------
createApp( App )
	.provide( NamespacesKey, namespaces )
	.use( router )
	.mount( '#app' );
