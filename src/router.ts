import { createRouter, createWebHistory, RouteLocationNormalized } from 'vue-router';
import StartView from './views/StartView.vue';
import ResultsView from './views/ResultsView.vue';

/**
 * Disallow navigation to ResultsView with invalid or missing "page" query
 * parameter. If a valid page param is not present, ?page=1 will be added
 * to the user's query.
 *
 * @param {RouteLocationNormalized} to
 * @return {RouteLocationNormalized|undefined}
 */
export function ensureValidPageNumber( to: RouteLocationNormalized ) {
	if ( to.query.page && Number( to.query.page ) && Number( to.query.page ) > 0 ) {
		return;
	} else {
		return {
			...to,
			query: {
				...to.query,
				page: '1'
			}
		};
	}
}

export const router = createRouter( {
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			component: StartView,
			name: 'Start'
		},
		{
			path: '/pages/:pageId?',
			component: ResultsView,
			name: 'Results',
			beforeEnter: [ ensureValidPageNumber ],
			props: ( route ) => {
				return {
					pageId: route.params.pageId,
					ns: route.query.ns,
					namespaceFilter: route.query.namespace_filter,
					page: Number( route.query.page )
				};
			}
		}
	]
} );
