import { Backlink, Namespace } from './types';

export function sortByLastTouched( a: Backlink, b: Backlink ) : number {
	return b.last_touched - a.last_touched;
}

export function sortByTitleAndNamespace( a: Backlink, b: Backlink ): number {
	if ( a.namespace === b.namespace ) {
		return a.title.toLowerCase().localeCompare( b.title.toLowerCase() );
	} else {
		return a.namespace < b.namespace ? -1 : 1;
	}
}

/**
 * Load the namespace data from enwiki. Needed to translate between
 * NS ids (numbers) and NS prefixes (strings).
 */
export async function fetchEnWikiNamespaces() {
	const endpoint = 'https://en.wikipedia.org/w/api.php';
	const params = '?action=query&format=json&meta=siteinfo&formatversion=2&siprop=namespaces&origin=*';
	const apiUrl = new URL( `${endpoint}${params}` );

	apiUrl.searchParams.set( 'origin', '*' );
	const url = apiUrl.toString();

	const response = await fetch( url );
	const data = await response.json();

	let nsList: Namespace[] = [];

	if ( data.query.namespaces ) {
		nsList = Object.keys( data.query.namespaces ).map( ( k ) => data.query.namespaces[ k ] );
	}

	return nsList;
}
